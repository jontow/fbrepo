#!/bin/sh

################################################################################
#
# 2019-12-09 -- jontow@unc.edu
#
# Examples:
#
# GIT_REMOTE="https://gitlab.com/jontow/fbrepo.git" sh awxtest.sh check
# DEBUG=true GIT_REMOTE="https://gitlab.com/jontow/fbrepo.git" sh awxtest.sh run
#
################################################################################

# http or https
URL_PROTO="http"

# FQDN, used to assemble URL
AWX_HOST="awx1.vdev"

# Default AWX credentials encoded here, substitute your own:
AUTH_TOKEN="${AUTH_TOKEN:-Basic YWRtaW46cGFzc3dvcmQ=}"

# Sleep between retries (seconds)
SLEEP_INTERVAL=1

# This number of retries constitutes a timeout (seconds)
JOB_TIMEOUT=30

# Wait this long before deleting temporary resources (seconds)
WAIT_BEFORE_DELETE=30

################################################################################

JOB_TYPE="$1"
if [ "${JOB_TYPE}" != "check" ] && \
   [ "${JOB_TYPE}" != "run" ]; then
    echo "Syntax: awxtest.sh <check|run> [git-origin] [playbook.yml]"
    exit 1
fi

if [ ! -d .git ]; then
    echo "ERROR: Current working directory not a git repository."
    exit 1
fi

CURL_DEBUG="-s"
if [ ! -z "${DEBUG}" ]; then
    CURL_DEBUG="-v"
fi

GIT_ORIGIN="origin"
if [ ! -z "$2" ]; then
    GIT_ORIGIN="$2"
fi
GIT_REMOTE="${GIT_REMOTE:-$(git remote get-url "${GIT_ORIGIN}")}"

GIT_BRANCH="$(git branch | awk '/^*/ {print $2}')"

PLAYBOOK="playbook.yml"
if [ ! -z "$3" ]; then
    PLAYBOOK="$3"
fi

###
#
# Create a new (TEMPORARY) project
#
echo "Creating project for ${GIT_REMOTE} ..."
project_out=$(curl "${CURL_DEBUG}" -X POST \
  "${URL_PROTO}://${AWX_HOST}/api/v2/projects/" \
  -H 'Accept: */*' \
  -H 'Accept-Encoding: gzip, deflate' \
  -H "Authorization: ${AUTH_TOKEN}" \
  -H 'Cache-Control: no-cache' \
  -H 'Connection: keep-alive' \
  -H 'Content-Type: application/json' \
  -H "Host: ${AWX_HOST}" \
  -H 'cache-control: no-cache' \
  -d "{
    \"name\":\"TMPTEST__${GIT_BRANCH}\",
    \"organization\":1,
    \"scm_type\":\"git\",
    \"base_dir\":\"/var/lib/awx/projects\",
    \"scm_url\":\"${GIT_REMOTE}\",
    \"scm_branch\":\"${GIT_BRANCH}\",
    \"scm_clean\":true,
    \"scm_delete_on_update\":true,
    \"scm_update_on_launch\":true,
    \"scm_update_cache_timeout\":\"60\",
    \"custom_virtualenv\":null
  }")
project_id=$(echo "${project_out}" | tr '\r\n' ' ' | jq .id)
if [ ! -z "${DEBUG}" ]; then
    echo "project out: ${project_out}"
fi
echo "  OK: project id: ${project_id}"

if ! echo "${project_id}" | grep -q -E '[0-9]+'; then
    echo "  ERROR: project creation failed:"
    exit 2
fi

################################################################################
#
# To move on, we need to confirm that the 'SCM Update' job was scheduled and
# completed successfully, or our job created later cannot run.  The following
# loop just iterates on the 'unified_jobs' waiting for a successful match.
#
echo "Waiting for Project ${project_id} SCM Update job to complete ..."
waiting_on_scm_update=
loop_count=0
scm_update_json=$(mktemp scm_update_json.XXXXX)
while [ -z "${waiting_on_scm_update}" ] && \
      [ "${loop_count}" -lt "${JOB_TIMEOUT}" ]; do
    # Sleep at the top of the loop to give better chance of first-run success
    sleep "${SLEEP_INTERVAL}"
    curl ${CURL_DEBUG} -X GET \
      "${URL_PROTO}://${AWX_HOST}/api/v2/unified_jobs/?page_size=20&order_by=-started&not__launch_type=sync" \
      -H 'Accept: */*' \
      -H 'Accept-Encoding: gzip, deflate' \
      -H "Authorization: ${AUTH_TOKEN}" \
      -H 'Cache-Control: no-cache' \
      -H 'Connection: keep-alive' \
      -H 'cache-control: no-cache' >"${scm_update_json}"
    if [ ! -z "${DEBUG}" ]; then
        echo "scm_update_json: ${scm_update_json}"
    fi
    ujobs_id=$(jq ".results[] | select(.summary_fields.unified_job_template.unified_job_type == \"project_update\" and .summary_fields.unified_job_template.name == \"TMPTEST__${GIT_BRANCH}\" and .status == \"successful\") | .id" "${scm_update_json}")
    if [ ! -z "${ujobs_id}" ]; then
        #break
        echo "  OK: SCM Update job completed successfully with job id: ${ujobs_id}"
	waiting_on_scm_update="no"
    fi
    loop_count=$((loop_count + 1))
done
rm "${scm_update_json}"

###
#
# Create a new (TEMPORARY) template
#
echo "Creating template ..."
template_out=$(curl "${CURL_DEBUG}" -X POST \
  "${URL_PROTO}://${AWX_HOST}/api/v2/job_templates/" \
  -H 'Accept: */*' \
  -H 'Accept-Encoding: gzip, deflate' \
  -H "Authorization: ${AUTH_TOKEN}" \
  -H 'Cache-Control: no-cache' \
  -H 'Connection: keep-alive' \
  -H 'Content-Type: application/json' \
  -H "Host: ${AWX_HOST}" \
  -H 'cache-control: no-cache' \
  -d "{
	\"name\":\"TMPTEST__${GIT_BRANCH}\",
	\"job_type\":\"${JOB_TYPE}\",
	\"inventory\":1,
	\"project\":${project_id},
	\"playbook\":\"${PLAYBOOK}\",
	\"verbosity\":0,
	\"job_tags\":\"\",
	\"skip_tags\":\"\",
	\"custom_virtualenv\":null,
	\"job_slice_count\":1,
	\"timeout\":0,
	\"diff_mode\":true,
	\"become_enabled\":true,
	\"allow_callbacks\":false,
	\"enable_webhook\":false,
	\"webhook_credential\":null,
	\"forks\":0,
	\"ask_diff_mode_on_launch\":false,
	\"ask_scm_branch_on_launch\":false,
	\"ask_tags_on_launch\":false,
	\"ask_skip_tags_on_launch\":false,
	\"ask_limit_on_launch\":false,
	\"ask_job_type_on_launch\":false,
	\"ask_verbosity_on_launch\":false,
	\"ask_inventory_on_launch\":false,
	\"ask_variables_on_launch\":false,
	\"ask_credential_on_launch\":false,
	\"extra_vars\":\"\",
	\"survey_enabled\":false
    }")
template_id=$(echo "${template_out}" | tr '\r\n' ' ' | jq .id)
if [ ! -z "${DEBUG}" ]; then
    echo "template out: ${template_out}"
fi
echo "  OK: template id: ${template_id}"

if ! echo "${template_id}" | grep -q -E '[0-9]+'; then
    echo "  ERROR: template creation failed:"
    exit 2
fi

###
#
# Create a new (TEMPORARY) job
#
echo "Creating job with template ${template_id} ..."
job_out=$(curl "${CURL_DEBUG}" -X POST \
  "${URL_PROTO}://${AWX_HOST}/api/v2/job_templates/${template_id}/launch/" \
  -H 'Accept: */*' \
  -H 'Accept-Encoding: gzip, deflate' \
  -H "Authorization: ${AUTH_TOKEN}" \
  -H 'Cache-Control: no-cache' \
  -H 'Connection: keep-alive' \
  -H "Host: ${AWX_HOST}" \
  -H 'cache-control: no-cache')
job_id=$(echo "${job_out}" | tr '\r\n' ' ' | jq .id)
if [ ! -z "${DEBUG}" ]; then
    echo "job out: ${job_out}"
fi
echo "  OK: job id: ${job_id}"

if ! echo "${job_id}" | grep -q -E '[0-9]+'; then
    echo "    ERROR: job launch failed:"
    exit 2
fi

################################################################################
#
# To move on, we need to confirm that our newly created job was scheduled and
# completed successfully, or our test has failed.  The following loop just
# iterates on the 'unified_jobs' waiting for a successful match.
#
echo "Waiting for job ${job_id} to complete ..."
waiting_on_job_run=
loop_count=0
job_run_json=$(mktemp job_run_json.XXXXX)
while [ -z ${waiting_on_job_run} ]; do
    # Sleep at the top of the loop to give better chance of first-run success
    sleep "${SLEEP_INTERVAL}"

    if [ "${loop_count}" -ge "${JOB_TIMEOUT}" ]; then
        echo "Job timeout: failure! (${JOB_TIMEOUT})"
	break
    fi

    curl ${CURL_DEBUG} -X GET \
      "${URL_PROTO}://${AWX_HOST}/api/v2/unified_jobs/?page_size=20&order_by=-started&not__launch_type=sync" \
      -H 'Accept: */*' \
      -H 'Accept-Encoding: gzip, deflate' \
      -H "Authorization: ${AUTH_TOKEN}" \
      -H 'Cache-Control: no-cache' \
      -H 'Connection: keep-alive' \
      -H 'cache-control: no-cache' >"${job_run_json}"
    if [ ! -z "${DEBUG}" ]; then
        echo "job_run_json: ${job_run_json}"
    fi

    ### Check for job success
    # shellcheck disable=SC2086
    ujobs_status=$(jq -r ".results[] | select(.summary_fields.unified_job_template.unified_job_type == \"job\" and .summary_fields.unified_job_template.name == \"TMPTEST__${GIT_BRANCH}\").status" "${job_run_json}")
    if [ ! -z "${ujobs_status}" ]; then
        case "${ujobs_status}" in
	  "successful"|"failed"|"error"|"canceled")
            echo "Job completed with status: ${ujobs_status}"
	    waiting_on_job_run="no"
	    break
	    ;;
	  *)
	    echo "WAITING: Job not completed, current status: ${ujobs_status}"
	    ;;
	esac
    fi

    loop_count=$((loop_count + 1))
done
rm "${job_run_json}"

echo "All known jobs completed: sleeping for ${WAIT_BEFORE_DELETE}sec before deleting jobs to allow for inspection"
echo ""
echo "Link to job output:"
echo ""
echo "  ${URL_PROTO}://${AWX_HOST}/#/jobs/playbook/${job_id}/"
echo ""
sleep "${WAIT_BEFORE_DELETE}"

echo "Deleting template_id ${template_id}"
curl "${CURL_DEBUG}" -X DELETE \
  "${URL_PROTO}://${AWX_HOST}/api/v2/job_templates/${template_id}/" \
  -H 'Accept: */*' \
  -H 'Accept-Encoding: gzip, deflate' \
  -H "Authorization: ${AUTH_TOKEN}" \
  -H 'Cache-Control: no-cache' \
  -H 'Connection: keep-alive' \
  -H 'cache-control: no-cache'

echo "Deleting  project_id ${project_id}"
curl "${CURL_DEBUG}" -X DELETE \
  "${URL_PROTO}://${AWX_HOST}/api/v2/projects/${project_id}/" \
  -H 'Accept: */*' \
  -H 'Accept-Encoding: gzip, deflate' \
  -H "Authorization: ${AUTH_TOKEN}" \
  -H 'Cache-Control: no-cache' \
  -H 'Connection: keep-alive' \
  -H 'cache-control: no-cache'

exit 0
